<?php
    $fichesFHF = new CligneFraisFHFs();
    $visiteurs = new Cvisiteurs();
    $col_ficheHF = $fichesFHF->FHFByUser();
?>

<div class="container">
    <table class="table">
    <thead>
        <tr>
        <th scope="col">Nom</th>
        <th scope="col">Prenom</th>
        <th scope="col">Nb. FhF</th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($col_ficheHF as $user => $element)
            {
        ?>
            <tr>
                <td><?= $visiteurs->ocollVisiteur[$user]->nom  ?></td>
                <td><?= $visiteurs->ocollVisiteur[$user]->prenom ?></td>
                <td><?= $element ?></td>
            </tr>
        <?php
            }
        ?>
    </tbody>
    </table>
</div>